#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctime>

using namespace std;

int main()
{
    const int n = 6;
    cout << "Our N = " << n << endl;
    cout << "Our array: \n";

    int array[n][n];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            array[i][j] = { i + j };
            cout << array[i][j] << ", ";
        }
        cout << '\n';
    }

    time_t t = time(NULL);
    tm* timePtr = localtime(&t);
    int day = timePtr->tm_mday;
    cout << "\nToday is the " << day << "th" << endl;

    int a = day % n;
    cout << "% = " << a << '\n' << endl;

    cout << "Result = ";
    int sum = 0;
    for (int j = 0; j < n; ++j)
    {
        cout << array[a-1][j] << ", ";
        sum += array[a - 1][j];
    }

    cout << "\nSum = " << sum;

    return 0;
}